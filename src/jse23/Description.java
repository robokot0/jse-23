package jse23;

 public class Description {
    private String parameterName = null;
    private String parameterTypeName = null;
    private Boolean hasValue = null;

     public String getParameterName() {
         return parameterName;
     }

     public void setParameterName(String parameterName) {
         this.parameterName = parameterName;
     }

     public String getParameterTypeName() {
         return parameterTypeName;
     }

     public void setParameterTypeName(String parameterTypeName) {
         this.parameterTypeName = parameterTypeName;
     }

     public Boolean getHasValue() {
         return hasValue;
     }

     public void setHasValue(Boolean hasValue) {
         this.hasValue = hasValue;
     }

     public Description(String parameterName, String parameterTypeName, Boolean hasValue) {
         this.parameterName = parameterName;
         this.parameterTypeName = parameterTypeName;
         this.hasValue = hasValue;
     }

     @Override
     public String toString() {
         return "Description{" +
                 "parameterName='" + parameterName + '\'' +
                 ", parameterTypeName='" + parameterTypeName + '\'' +
                 ", hasValue=" + hasValue +
                 '}';
     }
 }
