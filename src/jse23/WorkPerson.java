package jse23;

import java.time.LocalDate;

public class WorkPerson extends Person{
    private String tn = "";

    public String getTn() {
        return tn;
    }

    public void setTn(String tn) {
        this.tn = tn;
    }

    public WorkPerson(String firstName, String lastName, LocalDate birthDate, String email, String tn) {
        super(firstName, lastName, birthDate, email);
        this.tn = tn;
    }
}
