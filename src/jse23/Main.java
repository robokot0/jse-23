package jse23;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) throws IllegalAccessException, IOException {
        List<Person> personList = new ArrayList<>();
        personList.add(new Person("FN1","LN1", LocalDate.now(),"emeail1"));
        personList.add(new Person("FN2","LN2", LocalDate.now(),""));
        personList.add(new Person("FN3","LN3", LocalDate.now(),null));

        DescriptionUtil descriptionUtil = new DescriptionUtil();
        List<List<Description>> descriptionsList;
        descriptionsList=descriptionUtil.getObjectsDescription(personList);
        System.out.println(descriptionsList.toString());

        descriptionUtil.toCSV(System.out,descriptionsList);

        personList.clear();//если закомментировать то будет IllegalArgumentException
        personList.add(new WorkPerson("WPFN4","WPLN4",LocalDate.now(),"WPemail4","WPTN4"));
        personList.add(new WorkPerson("WPFN5","WPLN5",LocalDate.now(),"WPemail5","WPTN5"));

        descriptionsList=descriptionUtil.getObjectsDescription(personList);
        System.out.println(descriptionsList.toString());

        descriptionUtil.toCSV(System.out,descriptionsList);


    }




}
