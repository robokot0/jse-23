package jse23;

import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.dataformat.csv.CsvGenerator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;


public class DescriptionUtil {
    public List<Description> getObjectDescription(List<Description> descriptions, Object o,Class clazz) throws IllegalAccessException {
        List<Description> descriptionList=null;
        if(descriptions==null) {
            descriptionList = new ArrayList<>();
        } else {
            descriptionList = descriptions;
        }
        if(clazz==null) {
            return descriptionList;
        }
        for (Field field : clazz.getDeclaredFields()) {
            field.setAccessible(true);
            descriptionList.add(new Description(field.getName(), field.getType().getName(), field.get(o)!=null));
        }
        descriptionList=getObjectDescription(descriptionList,o,clazz.getSuperclass());
        return descriptionList;
    }
    public List<List<Description>> getObjectsDescription(List<? extends Object> objectList) throws IllegalAccessException {
        final List<List<Description>> descriptionLists = new ArrayList<>();
        if (objectList.size()==0) return descriptionLists;
        String currentType = objectList.get(0).getClass().getName();
        for (Object o : objectList) {
            if(!currentType.equals(o.getClass().getName())) throw  new IllegalArgumentException("getObjectsDescription Объекты в списке должны быть одного типа "+currentType+" отличается от "+o.getClass().getName());
            descriptionLists.add(getObjectDescription(null,o,o.getClass()));
        }
        return descriptionLists;
    }
    public void toCSV(OutputStream outputStream, List<List<Description>> descriptionsList) throws IOException {
        CsvMapper csvMapper=new CsvMapper();
        csvMapper.configure(CsvGenerator.Feature.ALWAYS_QUOTE_STRINGS,true);
        CsvSchema csvSchema = CsvSchema.builder().addColumn("parameterName").addColumn("parameterTypeName").addBooleanColumn("hasValue").setUseHeader(true).build();
        ObjectWriter objectWriter = csvMapper.writerFor(descriptionsList.getClass()).with(csvSchema);
        objectWriter.writeValues(outputStream).writeAll(descriptionsList);
    }
}
